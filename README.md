[![build status](https://img.shields.io/badge/build-passing-brightgreen)](https://gitlab.com/peterstoyanov/insight-hub/pipelines)
[![Swagger](https://img.shields.io/badge/swagger-valid-brightgreen)]()
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Insight Hub

![100x100](Logo.JPG)

Insight Hub version 1.0 is a web application that aims to help business needs and improve their workflow via reports, concerning different industry trends.  For every company, it is important to receive the necessary information as fast as possible. That’s why Insight Hub is the perfect place to find proved and effective solutions.

**[Click](https://trello.com/b/7hJKnbwS/insight-hub) to view a board in Trello.**

***The project was created by [Petar Stoyanov](https://gitlab.com/peterstoyanov) and  [Yani Yordanov](https://gitlab.com/YaniLtd)***

[[_TOC_]]

## Structure and content

 1. Ability to create business reports divided by industries
 2. Edit your reports at any time and keep track of their status
 3. Easy and convenient search engine
 4. Possibility to sort reports by selected criteria
 5. Suitable filter and pagination functionalities
 6. Always be up to date by viewing the latest reports
 7. Make sure to check the most popular ones
 8. Subscribe for email notifications concerning new reports
 9. Download the reports that are of interest to you.
 10. Also keep track of their status and re-download their updated version 
 
## Main advantages discovered

>Logically distributed assemblies-data, services, web and web API\
A convenient frontend design\
The majority of the code complies with the SOLID principles\
There is dynamic chart available, which is updated after every download\
Search functionality using asynchronous requests with AJAX\
Using useful and convenient approach for converting data into PDF format\
The application has appropriate customer and server validations\
The application is integrated with CI/DI\
The API is documented with swagger

##  Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software 

 - [Visual Studio 2019](https://visualstudio.microsoft.com/vs/) 
 - [Entity Framework](https://www.nuget.org/packages/EntityFramework/)
 - [SQL Server](https://www.microsoft.com/en-us/sql-server/)

## Used Technologies

- ASP.NET Core 3.1
- Entity Framework Core
- MS SQL Server
- HTML
- CSS
- Bootstrap
- Fusion Charts - JS charting library
- AJAX
- Azure
- MimeKit


## Database

![Database](/InsightHub/InsightHub/wwwroot/images/DB.PNG)


``please wait for GIF loading ``
## Public Part 

Accessible content that doesn’t require registration. 
A landing page has a list of three categories:
> Featured (reports selected by the admins)
Popular (the reports with the most downloads)
New (the newest reports)

Users that are not logged are able to see details for each report:\
>Name\
>Summary\
>Creator\
>Tags\
>Number of downloads\
>Download button (that redirects to the sign-in page)

Users that are not logged are able to filter reports by name, industry, tags, and sort by the following fields:
>Report Name\
>Nnumber of downloads\
>Upload date
 
![Public Part](/InsightHub/InsightHub/wwwroot/images/ReadMe/1. Public Part.gif)

## Registration Process

Registered users can choose in one of three roles – authors, customers, or admins.

To register, anonymous users must provide their first name, last name, email, type of registration (author or customer), phone number and to specify their password. They should be able to login to the system only after admin approves their registration request.

![Registration Process](/InsightHub/InsightHub/wwwroot/images/ReadMe/2. Registration-Process.gif)

## Admin

System administrators can administer all major information objects in the system. The administrators have the following capabilities:

>Approve new reports\
Delete/Edit all reports\
Approve user accounts\
Disable users accounts

![Admin](/InsightHub/InsightHub/wwwroot/images/ReadMe/3. Admin.gif)

## Customer

>Customers are able to browse and download all reports. Also are able to subscribe for email notification (per industry) in case a new report is released (I.e. approved by admins and visible to customers). Additionally, customers have a private area in the web application accessible after successful login, they could see all reports that they have downloaded.

![Customer](/InsightHub/InsightHub/wwwroot/images/ReadMe/4. Customer.gif)

## Author

>Authors have a private area in the web application accessible after successful login, where they could see all reports that are owned by the currently logged user.\
>Additionally, the author able to: 
>Delete/Update/Create their own report

Each report must have the following data:
>Name\
>Description\ 
>Author\ 
>Industry (IT, Healthcare, Government, Finance, Defence, etc.)\ 
>Tags (e.g. Audit & Risk, Customer Service & Support, Human Resources, Marketing, Sales, etc.) >Binary content (the report document itself)\ 

Once report is created it is “pending” state until the administrator approves it. The report is visible to customers only if it is approved.

![Customer](/InsightHub/InsightHub/wwwroot/images/ReadMe/5. Author.gif)

## REST API

![Customer](/InsightHub/InsightHub/wwwroot/images/ReadMe/6. Rest-API.gif)

## Open source software to collaborate on code

We want to make contributing to this project as easy and transparent as possible, whether it's:\

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

We Use [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) , so all code changes happen through pull requests. Pull requests are the best way to propose changes to the codebase. We actively welcome your pull requests:

1. Fork the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code lints.
6. Issue that pull request!


## Getting help

Please see [Getting help for GitLab](https://about.gitlab.com/getting-help/) on our website for the many options to get help.
