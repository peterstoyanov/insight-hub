﻿using InsightHub.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    public class ReportConfig : IEntityTypeConfiguration<Report>
    {
        public void Configure(EntityTypeBuilder<Report> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Industry)
                .WithMany(x => x.Reports)
                .HasForeignKey(x => x.IndustryId);

            builder.HasOne(x => x.Author)
                .WithMany(x => x.Reports)
                .HasForeignKey(x => x.AuthorId);

 
        }
    }
}
