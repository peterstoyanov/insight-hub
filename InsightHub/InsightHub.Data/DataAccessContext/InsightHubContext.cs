﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using InsightHub.Data.DatabaseSeeder;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data.Entities;
using System.Reflection;

namespace InsightHub.Data.DataAccessContext
{
    public class InsightHubContext : IdentityDbContext<User, Role, int>
    {
        public InsightHubContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Report> Reports { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<UserIndustries> UserIndustries { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ReportTags> ReportTags { get; set; }
        public DbSet<ReportDownloaders> ReportDownloaders { get; set; }
        public DbSet<Ban> Bans { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            Assembly assemblyWithConfigurations = Assembly.GetExecutingAssembly();
            builder.ApplyConfigurationsFromAssembly(assemblyWithConfigurations);
            builder.Seeder();
            base.OnModelCreating(builder);
        }
    }
}
