﻿using System.ComponentModel.DataAnnotations.Schema;
using InsightHub.Data.Entities.Abstract;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Data.Entities
{
    public class Report : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public Industry IndustryImg { get; set; }
        public int IndustryId { get; set; }
        public Industry Industry { get; set; }
        public IEnumerable<ReportTags> ReportTags { get; set; } = new HashSet<ReportTags>();
        public IEnumerable<ReportDownloaders> ReportDownloaders { get; set; } = new HashSet<ReportDownloaders>();
        public string BinaryContent { get; set; } 
        public int DownloadsCnt { get; set; } 
        public DateTime UploadedOn { get; set; } = DateTime.UtcNow;
        public bool IsPending { get; set; }
        public bool IsFeatured { get; set; }
    }
}
