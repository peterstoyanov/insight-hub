﻿namespace InsightHub.Data.Entities.Blob
{

    public class UploadContentRequest
    {
        public string Content { get; set; }
        public string FileName { get; set; }
    }
}
