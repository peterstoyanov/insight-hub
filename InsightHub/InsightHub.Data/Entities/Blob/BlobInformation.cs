﻿using System.IO;

namespace InsightHub.Data.Entities.Blob
{
    public class BlobInformation
    {
        public BlobInformation(Stream content, string contentType)
        {
            Content = content;
            ContentType = contentType;
        }

        public Stream Content { get; }
        public string ContentType { get; }
    }
}
