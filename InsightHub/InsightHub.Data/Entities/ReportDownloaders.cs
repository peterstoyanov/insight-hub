﻿namespace InsightHub.Data.Entities
{
    public class ReportDownloaders
    {
        public int ReportId { get; set; }
        public Report Report { get; set; }
        public int DownloaderId { get; set; }
        public User Downloader { get; set; }
        public int TimesDownloaded { get; set; }
    }
}
