﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Entities
{
    public class ReportTags
    {
        public int ReportId { get; set; }
        public Report Report { get; set; }
        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
