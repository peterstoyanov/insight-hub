﻿using System.Collections.Generic;
using InsightHub.Data.Entities;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A model class that is used to take/send Tag concerning data from/to the browser 
    /// </summary>
    public class TagViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ReportTags> Reports { get; set; }       
    }
}
