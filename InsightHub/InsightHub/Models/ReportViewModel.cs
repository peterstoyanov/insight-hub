﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;
using System;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A model class that is used to take/send Report concerning data from/to the browser 
    /// </summary>
    public class ReportViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Report name field is required!")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string Name { get; set; }
        [DisplayName("Text of the report")]
        [Required(ErrorMessage = "Report text field is required!")]
        [StringLength(3000, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        public string Description { get; set; }
        [Required(ErrorMessage = "Summary field is required!")]
        [StringLength(300, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        public string Summary { get; set; }
        public int AuthorId { get; set; }
        [DisplayName("Author")]
        public string AuthorName { get; set; }
        [Required]
        public int IndustryId { get; set; }
        public string IndustryImg { get; set; }
        [DisplayName("Industry")]
        public string IndustryName { get; set; }
        [DisplayName("Tags")]
        public IEnumerable<string> TagNames { get; set; }
        public IEnumerable<string> NamesOfDownloaders { get; set; }
        public string BinaryContent { get; set; }
        [DisplayName("Downloads count")]
        public int DownloadsCnt { get; set; }
        [DisplayName("Uploaded On")]
        public DateTime UploadedOn { get; set; }
        public bool IsPending { get; set; }
        public bool IsFeatured { get; set; }
        //Select List
        public IEnumerable<SelectListItem> Tags { get; set; }
        public string[] SelectedTags { get; set; }
    }
}
