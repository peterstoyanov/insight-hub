﻿using InsightHub.Web.WebMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InsightHub.Web.Models;
using NToastNotify;
using System;

namespace InsightHub.Web.Controllers.Admin
{
    /// <summary>
    /// This class contains methods for working with reports
    /// that only the admin can invoke
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class AdminReportController : Controller
    {
        private readonly IReportService reportService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<ReportDTO, ReportViewModel> reportMapper;
        private readonly IMailer mailer;

        public AdminReportController(IReportService reportService,
            IViewModelMapper<ReportDTO, ReportViewModel> reportMapper,
            IToastNotification toastNotification, IMailer mailer)
        {
            this.reportService = reportService ?? throw new ArgumentNullException(nameof(reportService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
            this.reportMapper = reportMapper ?? throw new ArgumentNullException(nameof(reportMapper));
            this.mailer = mailer ?? throw new ArgumentNullException(nameof(mailer));
        }

        [HttpGet]
        public async Task<IActionResult> GetFullReportForApproval(int? id)
        {
            var fullReport = (await this.reportService.GetReportAsync(id));
            var report = this.reportMapper.MapFrom(fullReport);
            return View(report);
        }

        [HttpGet]
        public async Task<IActionResult> GetReportsForApproval()
        {
            var pendingReports = (await this.reportService.GetPendingReportsAsync());          
            var pendingReportsVM = this.reportMapper.MapFrom(pendingReports);
            ViewData["pendingReportsVM"] = pendingReportsVM;

            return View();
        }

        public async Task<IActionResult> ApproveReportConfirm(int? id)
        {
            try
            {
                var reports = await this.reportService.AproveReportAsync(id);
                this.toastNotification.AddInfoToastMessage("User will be notified by email from Insight Hub Team");
                this.toastNotification.AddSuccessToastMessage("Report was successfully approved");
                return RedirectToAction("GetReportsForApproval");
            }

            catch (Exception e)
            {
                this.toastNotification.AddWarningToastMessage(e.Message);
                return View();
            }
        }

        public async Task<IActionResult> RejectReportConfirm(int? id)
        {
            try
            {
                await this.reportService.RejectReportAsync(id);
                this.toastNotification.AddInfoToastMessage("User will be notified by email from Insight Hub Team");
                this.toastNotification.AddWarningToastMessage("The request for report approval was rejected");
                return RedirectToAction("GetReportsForApproval");
            }

            catch (Exception e)
            {
                this.toastNotification.AddWarningToastMessage(e.Message);
                return View();
            }
        }

        [HttpGet]
        public IActionResult GetReportsOfUser()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetReportsOfUser(int id)
        {
            try
            {
                var reports = await this.reportService.GetReportsOfUserAsync(id);
                var reportsVM = this.reportMapper.MapFrom(reports);
                return View(reportsVM);
            }

            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> FeaturedReports()
        {
            var reports = await this.reportService.GetAprovedReportsAsync();
            var reportsVM = this.reportMapper.MapFrom(reports);

            return View(reportsVM);
        }

        public async Task<IActionResult> FeatureReport(int? id)
        {
            try
            {
                var featureReport = await this.reportService.FeatureReportAsync(id);
                this.toastNotification.AddSuccessToastMessage("Report was successfully featured in Home Page");
                return RedirectToAction("FeaturedReports");
            }

            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return RedirectToAction("FeaturedReports");
            }
        }

        public async Task<IActionResult> StopFeaturingReport(int? id)
        {
            try
            {
                var report = await this.reportService.StopFeaturingReportAsync(id);
                this.toastNotification.AddSuccessToastMessage("Report was successfully removed from the Homepage");
                return RedirectToAction("FeaturedReports");
            }

            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return RedirectToAction("FeaturedReports");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound("Id cannot be null or empty");
            }

            var report = await this.reportService.GetReportAsync(id.Value);

            if (report == null)
            {
                return NotFound("Report cannot be null or empty");
            }

            return View(reportMapper.MapFrom(report));
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var report = await this.reportService.DeleteReportAsync(id);
            this.toastNotification.AddSuccessToastMessage("Report successfully deleted");
            return RedirectToAction("GetAllReports", "AnonymousReport");
        }
    }
}



 