﻿using Microsoft.AspNetCore.Http.Extensions;
using InsightHub.Web.WebMappers.Contracts;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InsightHub.Web.Models;
using NToastNotify;
using System;
using Microsoft.AspNetCore.Authorization;

namespace InsightHub.Web.Controllers.Admin
{
    /// <summary>
    /// This class contains methods for working with user
    /// that only the admin can invoke
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class AdminUserController : Controller
    {
        private readonly IMailer mailer;
        private readonly IBanService banService;
        private readonly IUserService userService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<BanDTO, BanViewModel> banMapper;
        private readonly IViewModelMapper<UserDTO, UserViewModel> userMapper;

        public AdminUserController(IUserService userService, IMailer mailer,
            IBanService banService, IToastNotification toastNotification, IViewModelMapper<UserDTO,
            UserViewModel> userMapper, IViewModelMapper<BanDTO, BanViewModel> banMapper)
        {
            this.mailer = mailer ?? throw new ArgumentNullException(nameof(mailer));
            this.banMapper = banMapper ?? throw new ArgumentNullException(nameof(banMapper));
            this.userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
            this.banService = banService ?? throw new ArgumentNullException(nameof(banService));
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
        }

        [HttpGet]
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                var user = await this.userService.GetUserAsync(id);
                return View(user);
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetApprovedUsers()
        {
            var unbannedUsers = await this.userService.GetAllValidUsersAsync();
            var bannedUsers = await this.banService.GetAllBannedUsersAsync();
            var unbannedUsersVM = this.userMapper.MapFrom(unbannedUsers);
            var bannedUserVM = this.userMapper.MapFrom(bannedUsers);
            ViewData["unbannedUsersVM"] = unbannedUsersVM;
            ViewData["bannedUsersVM"] = bannedUserVM;

            return View();
        }

        [HttpGet]
        public IActionResult BanUser()
        {

            return View();
        }
 
        [HttpPost]
        public async Task<IActionResult> BanUser(BanViewModel banVM)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    this.toastNotification.AddErrorToastMessage("Model state is not valid.");
                    return RedirectToAction(nameof(GetApprovedUsers));
                }

                string url = Request.GetDisplayUrl();
                var tokens = url.Split('/');
                var userId = int.Parse(tokens[tokens.Length - 1]);
                banVM.UserId = userId;

                var banDTO = this.banMapper.MapFrom(banVM);
                await this.banService.CreateBanAsync(banDTO);

                this.toastNotification.AddSuccessToastMessage("Ban was successfully created");
                return RedirectToAction("GetApprovedUsers");
            }
            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return View();
            }
        }
   
        public async Task<IActionResult> UnbanUserConfirm(int? id)
        {
            try
            {
                if (id == null)
                {
                    this.toastNotification.AddErrorToastMessage("Id cannot be null");
                    return RedirectToAction(nameof(GetApprovedUsers));
                }

                await this.banService.RemoveBanAsync(id);

                this.toastNotification.AddSuccessToastMessage("Ban was successfully removed");
                return RedirectToAction("GetApprovedUsers");
            }
            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            var report = await this.userService.DeleteUserAsync(id);
            this.toastNotification.AddAlertToastMessage("User successfully deleted");
            return RedirectToAction(nameof(GetApprovedUsers));
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(int? id)
        {
            if (id == null)
            {
                return NotFound("Id cannot be null or empty");
            }

            var user = await this.userService.GetUserAsync(id.Value);

            if (user == null)
            {
                return NotFound("User cannot be null or empty");
            }

            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> GetUsersForApproval()

        {
            var pendingUsers = (await this.userService.GetPendingUsersAsync());
            var pendingUsersVM = this.userMapper.MapFrom(pendingUsers);
            ViewData["pendingUsersVM"] = pendingUsersVM;

            return View();
        }

        public async Task<IActionResult> ApproveUserConfirm(int? id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError(string.Empty, "Model state is not valid.");
                }

                var pendingUsers = await this.userService.AproveUserAsync(id);

                this.toastNotification.AddInfoToastMessage("User will be notified by email from Insight Hub Team");
                this.toastNotification.AddSuccessToastMessage("Registration was successfully approved");
                return RedirectToAction("GetUsersForApproval");
            }
            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return View();
            }
        }

        public async Task<IActionResult> RejectUserConfirm(int? id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError(string.Empty, "Model state is not valid.");
                }

                //TODO: hard delete
                var pendingUsers = await this.userService.GetUserAsync(id);
                await this.userService.DeleteUserAsync(id);

                this.toastNotification.AddInfoToastMessage("User will be notified by email from Insight Hub Team");
                this.toastNotification.AddWarningToastMessage("Account registration was rejected");
                await mailer.UserRejected(pendingUsers.Email);
                return RedirectToAction("GetUsersForApproval");
            }
            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return View();
            }
        }
    }
}
