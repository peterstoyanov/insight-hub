﻿using InsightHub.Web.WebMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using InsightHub.Web.Models;
using NToastNotify;
using System;

namespace InsightHub.Web.Controllers.Customer
{
    [Authorize(Roles = "Customer")]
    public class CustomerSubscriptionController : Controller
    {
        private readonly IReportService reportService;
        private readonly ISubscriptionService subscriptionService;
        private readonly IIndustryService industryService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<IndustryDTO, IndustryViewModel> industryMapper;

        public CustomerSubscriptionController(IReportService reportService,
            IViewModelMapper<IndustryDTO, IndustryViewModel> industryMapper,
            IToastNotification toastNotification, ISubscriptionService subscriptionService,
            IIndustryService industryService)
        {
            this.reportService = reportService ?? throw new ArgumentNullException(nameof(reportService));
            this.subscriptionService = subscriptionService ?? throw new ArgumentNullException(nameof(subscriptionService));
            this.industryService = industryService ?? throw new ArgumentNullException(nameof(industryService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
            this.industryMapper = industryMapper ?? throw new ArgumentNullException(nameof(industryMapper));
        }

        [HttpGet]
        public IActionResult GetNonSubscriptionsOfUser()

        {
            return View();
        }

        public async Task<IActionResult> SubscribeConfirm(int? id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError(string.Empty, "Model state is not valid.");
                }
                var loggedUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                await subscriptionService.SubscribeAsync(loggedUserId, id);

                this.toastNotification.AddSuccessToastMessage("Thank your for your subscription. " +
                    "The newsletter will be sent by Insight Team to your e-mail address.");
                return RedirectToAction("GetNonSubscriptionsOfUser");
            }
            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return View();
            }
        }

        [HttpGet]
        public IActionResult GetSubscriptionsOfUser()

        {
            return View();
        }

        public async Task<IActionResult> UnsubscribeConfirm(int? id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError(string.Empty, "Model state is not valid.");
                }

                var loggedUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                await subscriptionService.HardDeleteSubscriptionAsync(loggedUserId, id);

                this.toastNotification.AddSuccessToastMessage("You have successfully unsubscribed " +
                    "You will no longer receive product updates and announcements");
                return RedirectToAction("GetSubscriptionsOfUser");
            }
            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return View();
            }
        }
    }
}
