﻿//using InsightHub.Web.WebMappers.Contracts;
//using InsightHub.Services.Contracts;
//using InsightHub.Services.DTOs;
//using Microsoft.AspNetCore.Mvc;
//using System.Threading.Tasks;
//using InsightHub.Web.Models;
//using NToastNotify;
//using System;

//namespace InsightHub.Web.Controllers.Author
//{
//    /// <summary>
//    /// This class contains methods for working with tags
//    /// invokable by the author
//    /// </summary>
//    public class AuthorTagController : Controller
//    {
//        private readonly ITagService tagService;
//        private readonly IToastNotification toastNotification;
//        private readonly IViewModelMapper<TagDTO, TagViewModel> tagMapper;

//        public AuthorTagController(ITagService tagService,
//            IToastNotification toastNotification, IViewModelMapper<TagDTO, TagViewModel> tagMapper)
//        {
//            this.tagService = tagService ?? throw new ArgumentNullException(nameof(tagService));
//            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
//            this.tagMapper = tagMapper ?? throw new ArgumentNullException(nameof(tagMapper));
//        }
//    }
//}
