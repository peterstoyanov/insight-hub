﻿using InsightHub.Web.WebMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using FusionCharts.Visualization;
using InsightHub.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using InsightHub.Services.DTOs;
using FusionCharts.DataEngine;
using System.Threading.Tasks;
using InsightHub.Web.Models;
using System.Diagnostics;
using InsightHub.Models;
using NToastNotify;
using System.Data;
using System;

namespace InsightHub.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly SignInManager<User> signInManager;
        private readonly UserManager<User> userManager;
        private readonly ILogger<HomeController> logger;
        private readonly IReportService reportService;
        private readonly IViewModelMapper<ReportDTO, ReportViewModel> reportMapper;

        public HomeController(SignInManager<User> signInManager, UserManager<User> userManager,
            ILogger<HomeController> logger, IReportService reportService, IToastNotification toastNotification,
            IViewModelMapper<ReportDTO, ReportViewModel> reportMapper)
        {
            this.reportMapper = reportMapper;
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.reportService = reportService;
            this.logger = logger;
        }
        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            //var user = await userManager.GetUserAsync(User);
            //var isSignedIn = signInManager.IsSignedIn(User);
            await GetLatestReports();
            await GetFeaturedReports();
            await DisplayChart();
            return View();
        }

        public async Task<IActionResult> DisplayChart()
        {
            DataTable ChartData = new DataTable();
            ChartData.Columns.Add("Most popular Reports", typeof(string));
            ChartData.Columns.Add("Users", typeof(double));
            var topReports = await reportService.GetTopReportsAsync();

            foreach (var item in topReports)
            {
                ChartData.Rows.Add(item.Name, item.DownloadsCnt);
            }

            // Create static source with this data table
            StaticSource source = new StaticSource(ChartData);
            // Create instance of DataModel class
            DataModel model = new DataModel();
            // Add DataSource to the DataModel
            model.DataSources.Add(source);
            // Instantiate Column Chart
            Charts.ColumnChart column = new Charts.ColumnChart("first_chart");
            // Set Chart's width and height
            column.Width.Pixel(700);
            column.Height.Pixel(400);

            // Set DataModel instance as the data source of the chart
            column.Data.Source = model;
            // Set Chart Title
            column.Caption.Text = "Most popular Reports";
            // set XAxis Text
            column.XAxis.Text = "Insight Hub Reports";
            // Set YAxis title
            column.YAxis.Text = "User";
            column.ThemeName = FusionChartsTheme.ThemeName.FUSION;
            column.Export.Enabled = true;
            column.Export.ExportedFileName = "fusioncharts.net_visualizations_exported_files";
            column.Export.Action = Exporter.ExportAction.DOWNLOAD;
            column.ThemeName = FusionChartsTheme.ThemeName.FUSION;
            // Render the chart to 'Literal1' literal control
            column.Values.Show = true;

            ViewData["Chart"] = column.Render();
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetFeaturedReports()
        {
            try
            {
                var featuredReports = await this.reportService.GetFeaturedReportsAsync();
                var featuredReportsVM = this.reportMapper.MapFrom(featuredReports);
                return View(featuredReportsVM);
            }

            catch (Exception)
            {
                return (StatusCode(404));
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetLatestReports()
        {
            try
            {
                var latestReports = await this.reportService.GetLatestReportsAsync();
                var latestReportsVM = this.reportMapper.MapFrom(latestReports);
                ViewData["LatestReports"] = latestReportsVM;
                return View();
            }

            catch (Exception)
            {
                return (StatusCode(404));
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
