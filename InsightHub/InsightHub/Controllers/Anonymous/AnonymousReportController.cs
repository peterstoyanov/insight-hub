﻿using InsightHub.Web.WebMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InsightHub.Web.Models;
using NToastNotify;
using System;

namespace InsightHub.Web.Controllers.Anonymous
{
    /// <summary>
    /// This class contains public part methods for working with reports
    /// </summary>
    [AllowAnonymous]
    public class AnonymousReportController : Controller
    {
        private readonly IReportService reportService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<ReportDTO, ReportViewModel> reportMapper;

        public AnonymousReportController(IReportService reportService,
            IViewModelMapper<ReportDTO, ReportViewModel> reportMapper,
            IToastNotification toastNotification)
        {
            this.reportService = reportService ?? throw new ArgumentNullException(nameof(reportService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
            this.reportMapper = reportMapper ?? throw new ArgumentNullException(nameof(reportMapper));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllReports()
        {
            try
            {
                var reports = await this.reportService.GetAprovedReportsAsync();
                var reportsVM = this.reportMapper.MapFrom(reports);

                return View(reportsVM);
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetReport(int id)
        {
            try
            {
                var report = await this.reportService.GetReportAsync(id);
                return View(report);
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound("Id cannot be null or empty");
            }

            var report = await this.reportService.GetReportAsync(id.Value);

            if (report == null)
            {
                return NotFound("Report cannot be null or empty");
            }

            return View(reportMapper.MapFrom(report));
        }
    }
}
