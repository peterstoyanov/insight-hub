﻿using InsightHub.Web.WebMappers.Contracts;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System.Linq;
using System;

namespace InsightHub.Web.WebMappers
{
    public class TagMapper : IViewModelMapper<TagDTO, TagViewModel>, ITagWebMapper
    {
        public TagViewModel MapFrom(TagDTO entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("DTO not found");
            }

            return new TagViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Reports = entity.Reports
            };
        }

        public IEnumerable<TagViewModel> MapFrom(IEnumerable<TagDTO> entities)
        {
            return entities.Select(x => MapFrom(x));
        }

        public TagDTO MapFrom(TagViewModel entityVM)
        {
            if (entityVM == null)
            {
                throw new ArgumentException("View Model not found");
            }

            return new TagDTO
            {
                Id = entityVM.Id,
                Name = entityVM.Name,
                Reports = entityVM.Reports
            };
        }

        public IEnumerable<TagDTO> MapFrom(ICollection<TagViewModel> entitiesVM)
        {
            return entitiesVM.Select(x => MapFrom(x));
        }
    }
}
