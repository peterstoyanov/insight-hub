﻿using System.Collections.Generic;

namespace InsightHub.Web.WebMappers.Contracts
{
    public interface IViewModelMapper<TDTO, TViewModel>
    {
        TViewModel MapFrom(TDTO entity);
        IEnumerable<TViewModel> MapFrom(IEnumerable<TDTO> entities);
        TDTO MapFrom(TViewModel entityVM);
        IEnumerable<TDTO> MapFrom(ICollection<TViewModel> entitiesVM);
    }
}
