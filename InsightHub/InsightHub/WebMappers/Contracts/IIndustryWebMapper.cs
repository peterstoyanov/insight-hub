﻿using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.WebMappers.Contracts
{
    public interface IIndustryWebMapper
    {
        IEnumerable<IndustryViewModel> MapFrom(IEnumerable<IndustryDTO> entities);
        IndustryViewModel MapFrom(IndustryDTO entity);
        public IndustryDTO MapFrom(IndustryViewModel entityVM);
        public IEnumerable<IndustryDTO> MapFrom(ICollection<IndustryViewModel> entitiesVM);
    }
}
