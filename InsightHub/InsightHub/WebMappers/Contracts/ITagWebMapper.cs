﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;

namespace InsightHub.Web.WebMappers.Contracts
{
    public interface ITagWebMapper
    {
        IEnumerable<TagViewModel> MapFrom(IEnumerable<TagDTO> entities);
        TagViewModel MapFrom(TagDTO entity);
        public TagDTO MapFrom(TagViewModel entityVM);
        public IEnumerable<TagDTO> MapFrom(ICollection<TagViewModel> entitiesVM);
    }
}
