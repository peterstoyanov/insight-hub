﻿using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.WebMappers.Contracts
{
    public interface IBanWebMapper
    {
        IEnumerable<BanViewModel> MapFrom(IEnumerable<BanDTO> entities);
        BanViewModel MapFrom(BanDTO entity);
        public BanDTO MapFrom(BanViewModel entityVM);
        public IEnumerable<BanDTO> MapFrom(ICollection<BanViewModel> entitiesVM);
    }
}
