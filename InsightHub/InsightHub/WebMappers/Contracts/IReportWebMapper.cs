﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;

namespace InsightHub.Web.WebMappers.Contracts
{
    public interface IReportWebMapper
    {
        IEnumerable<ReportViewModel> MapFrom(IEnumerable<ReportDTO> entities);
        ReportViewModel MapFrom(ReportDTO entity);
        public ReportDTO MapFrom(ReportViewModel entityVM);
        public IEnumerable<ReportDTO> MapFrom(ICollection<ReportViewModel> entitiesVM);
    }
}