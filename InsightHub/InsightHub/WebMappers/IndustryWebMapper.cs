﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System.Linq;
using System;

namespace InsightHub.Web.WebMappers.Contracts
{
    public class IndustryWebMapper : IViewModelMapper<IndustryDTO, IndustryViewModel>, IIndustryWebMapper
    {
        public IndustryViewModel MapFrom(IndustryDTO entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("DTO not found");
            }

            return new IndustryViewModel
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public IEnumerable<IndustryViewModel> MapFrom(IEnumerable<IndustryDTO> entities)
        {
            return entities.Select(x => MapFrom(x));
        }

        public IndustryDTO MapFrom(IndustryViewModel entityVM)
        {
            if (entityVM == null)
            {
                throw new ArgumentException("View Model not found");
            }

            return new IndustryDTO
            {
                Id = entityVM.Id,
                Name = entityVM.Name
            };
        }

        public IEnumerable<IndustryDTO> MapFrom(ICollection<IndustryViewModel> entitiesVM)
        {
            return entitiesVM.Select(x => MapFrom(x));
        }
    }
}
