﻿using InsightHub.Services.DTOs;

namespace InsightHub.WebApi.Utilities
{
    public interface IAuthorizeService
    {
        void ByRole(UserDTO userDTO, string roles);
    }
}