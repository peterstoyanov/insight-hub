﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Authorization;
using InsightHub.WebApi.Utilities;
using System.Web;

namespace InsightHub.Web.Rest_Api
{
    [Route("api/[controller]s")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportService reportService;
        private readonly IUserService userService;
        private readonly IBlobService blobService;
        private readonly IAuthorizeService authorize;

        public ReportController(IReportService reportService, IBlobService blobService,
            IUserService userService, IAuthorizeService authorize)
        {
            this.reportService = reportService ?? throw new ArgumentNullException(nameof(reportService));
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.blobService = blobService ?? throw new ArgumentNullException(nameof(blobService));
            this.authorize = authorize ?? throw new ArgumentNullException(nameof(authorize));
        }
        [HttpGet("")]
        public async Task<IActionResult> GetApprovedReports()
        {
            try
            {
                var reports = await this.reportService.GetAprovedReportsAsync();
                return Ok(reports);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
        [HttpGet("latest")]
        public async Task<IActionResult> GetLatestReports()
        {
            try
            {
                var reports = await this.reportService.GetLatestReportsAsync();
                return Ok(reports);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
        [HttpGet("featured")]
        public async Task<IActionResult> GetFeaturedReports()
        {
            try
            {
                var reports = await this.reportService.GetFeaturedReportsAsync();
                return Ok(reports);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
        [HttpGet("top")]
        public async Task<IActionResult> GetTopReports()
        {
            try
            {
                var reports = await this.reportService.GetTopReportsAsync();
                return Ok(reports);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
        [HttpGet("pending")]
        public async Task<IActionResult> GetPendingReports([FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                var reports = await this.reportService.GetPendingReportsAsync();
                return Ok(reports);
            }
            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApprovedReport(int id)
        {
            try
            {
                var report = await this.reportService.GetApprovedReportAsync(id);
                return Ok(report);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("writtenbyme")]
        public async Task<IActionResult> GetReportsWrittenByMe([FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Author,Admin");
                var report = await this.reportService
                    .GetReportsOfUserAsync(requestingUser.Id);

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("writtenby/{userId}")]
        public async Task<IActionResult> GetReportsWrittenBy(int userId, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                var report = await this.reportService
                    .GetReportsOfUserAsync(userId);

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("all")]
        public async Task<IActionResult> GetAllReports([FromQuery] string token)
        {
            try
            {

                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                var reports = await this.reportService.GetAllReportsAsync();

                return Ok(reports);
            }
            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }


        }
        [HttpGet("pending/{id}")]
        public async Task<IActionResult> GetPendingReport(int id, [FromQuery] string token)
        {
            try
            {
                var report = await this.reportService.GetPendingReportAsync(id);
                var requestingUser = await userService.GetUserByTokenAsync(token);
                if (requestingUser.Id == report.AuthorId)
                    authorize.ByRole(requestingUser, "Author,Admin");
                else
                    authorize.ByRole(requestingUser, "Admin");

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
       
        [HttpGet("downloadedbyme")]
        public async Task<IActionResult> GetReportsDownloadedByMe([FromQuery] string token) 
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Customer,Admin");
                var report = await this.reportService
                    .GetDownloadedReportsByUserAsync(requestingUser.Id);

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("downloadedby/{userId}")]
        public async Task<IActionResult> GetReportsDownloadedByMe(
            int userId, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                var report = await this.reportService
                    .GetDownloadedReportsByUserAsync(userId);

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateReport( 
            [FromQuery] string token, [FromBody] ReportDTO reportDTO)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin,Author");
                reportDTO.AuthorId = requestingUser.Id;
                var report = await this.reportService.CreateReportAsync(reportDTO);

                return Created("", report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //DownloadReportAsync downloader id, report id
        [HttpGet("download/{id}")]
        public async Task<IActionResult> DownloadReport(int id, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Customer,Admin");
                var report = await this.reportService.DownloadReportAsync(requestingUser.Id, id);
                var data = await blobService.GetBlobAsync(report.Name + ".pdf");


                return File(data.Content, data.ContentType);
                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("approve/{id}")]
        public async Task<IActionResult> AproveReport(int id, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                var report = await this.reportService.AproveReportAsync(id);

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPut("my/{id}")]
        public async Task<IActionResult> UpdateMyReport(int id, 
            [FromBody] ReportDTO reportDTO, [FromQuery] string token)
        {
            try
            {
                var reportToBeUpdated = await this.reportService.GetApprovedReportAsync(id);
                var requestingUser = await userService.GetUserByTokenAsync(token);

                if (reportToBeUpdated.AuthorId != requestingUser.Id)
                    throw new ArgumentException("The report to be updated is not yours!!");

                authorize.ByRole(requestingUser, "Admin,Author");
                reportDTO.Id = id;
                var report = await this.reportService.UpdateReportAsync(reportDTO);

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateReport(int id,
            [FromBody] ReportDTO reportDTO, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                reportDTO.Id = id;
                var report = await this.reportService.UpdateReportAsync(reportDTO);

                return Ok(report);
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReport(int id, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                var report = await this.reportService.DeleteReportAsync(id);

                return Ok("Successfully deleted");
            }

            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
