﻿using InsightHub.Services.Mappers.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Linq;
using System;

namespace InsightHub.Services.Mappers
{
    /// <summary>
    /// Used to map from Report => ReportDTO
    /// </summary>
    public class ReportMapper : IDTOMapper<Report, ReportDTO>
    {
        public ReportDTO MapFrom(Report entity)
        {
            var author = entity.Author ??
                throw new ArgumentNullException("Author not included");
            var industry = entity.Industry ??
                throw new ArgumentNullException("Industry not included");
            _ = entity.ReportTags == null ?
                throw new ArgumentNullException("ReportTags not included") : true;
            if (entity.ReportTags.Count() > 0)
            {
                _ = entity.ReportTags.First() == null ?
                throw new ArgumentNullException("Tags not then included") : true;
            }
            _ = entity.ReportDownloaders == null ?
                throw new ArgumentNullException("ReportDownloaders not included") : true;
            if (entity.ReportDownloaders.Count() > 0)
            {
                _ = entity.ReportDownloaders.First().Downloader == null ?
                throw new ArgumentNullException("ReportDownloaders not included") : true;
            }
            
            var tagNames = entity.ReportTags.Select(x => x.Tag.Name);
            var namesOfDownloaders = entity.ReportDownloaders.Select(x => x.Downloader.Name);
            var downloadsCnt = entity.ReportDownloaders.Select(x => x.TimesDownloaded).Sum();
            return new ReportDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Summary = entity.Summary,
                AuthorId = entity.AuthorId,
                AuthorName = author.Name,
                IndustryId = entity.IndustryId,
                IndustryImg = industry.ImgUrl,
                IndustryName = industry.Name,
                TagNames = tagNames,
                DownloadsCnt = downloadsCnt,
                NamesOfDownloaders = namesOfDownloaders,
                BinaryContent = entity.BinaryContent,
                UploadedOn = entity.UploadedOn,
                IsPending = entity.IsPending,
                IsFeatured = entity.IsFeatured
            };
        }

        public IEnumerable<ReportDTO> MapFrom(IEnumerable<Report> entities)
        {
            return entities.Select(x => MapFrom(x));
        }
    }
}
