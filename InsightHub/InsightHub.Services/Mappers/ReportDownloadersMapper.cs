﻿using InsightHub.Services.Mappers.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Services.Mappers
{
    /// <summary>
    /// Used to map from ReportDownloaders => ReportDownloadersDTO
    /// </summary>
    public class ReportDownloadersMapper : IDTOMapper<ReportDownloaders, ReportDownloadersDTO>
    {
        public ReportDownloadersDTO MapFrom(ReportDownloaders entity)
        {
            return new ReportDownloadersDTO
            {
                ReportId = entity.ReportId,
                Report = entity.Report,
                DownloaderId = entity.DownloaderId,
                Downloader = entity.Downloader
            };
        }

        public IEnumerable<ReportDownloadersDTO> MapFrom(IEnumerable<ReportDownloaders> entities)
        {
            return entities.Select(x => MapFrom(x));
        }
    }
}
