﻿using InsightHub.Services.Mappers.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Services.Mappers
{
    /// <summary>
    /// Used to map from Tag => TagDTO
    /// </summary>
    public class TagMapper : IDTOMapper<Tag, TagDTO>
    {
        public TagDTO MapFrom(Tag entity)
        {
            return new TagDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Reports = entity.Reports
            };
        }

        public IEnumerable<TagDTO> MapFrom(IEnumerable<Tag> entities)
        {
            return entities.Select(x => MapFrom(x));
        }
    }
}
