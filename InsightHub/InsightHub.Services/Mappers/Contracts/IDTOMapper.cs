﻿using System.Collections.Generic;

namespace InsightHub.Services.Mappers.Contracts
{
    public interface IDTOMapper<TEntity, TDTO>
    {
        /// <summary>
        /// Maps the properties from a given entity to its respective DTO model
        /// </summary>
        TDTO MapFrom(TEntity entity);
        /// <summary>
        /// Maps the properties from a given entity to its respective DTO model
        /// for all the entities in the collection
        /// </summary>
        IEnumerable<TDTO> MapFrom(IEnumerable<TEntity> entities);
    }
}
