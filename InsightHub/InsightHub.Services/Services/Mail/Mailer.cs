﻿using InsightHub.Data.Entities.Mail;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using System;

namespace InsightHub.Services.Services.Mail
{
    /// <summary>
    /// Contains logics required for sending emails 
    /// </summary>
    public class Mailer : IMailer
    {
        private readonly SmtpSettings _smtpSettings;
        private readonly IWebHostEnvironment _env;

        public Mailer(IOptions<SmtpSettings> smtpSettings, IWebHostEnvironment env)
        {
            _smtpSettings = smtpSettings.Value;
            _env = env;
        }

        public async Task EmailConfirmed(string email) 
        {
            await SendEmailAsync(email, "Confimation", EmailConfirmed());
        }
        public async Task ReportAproved(string email, string reportName) 
        {
            await SendEmailAsync(email, "Report approval", ReportAproved(reportName));
        }
        public async Task UserAproved(string email) 
        {
            await SendEmailAsync(email, "Account approval", UserAproved());
        }
        public async Task ReportRejected(string email, string reportName) 
        {
            await SendEmailAsync(email, "Report rejected", ReportRejected(reportName));
        }
        public async Task UserRejected(string email) 
        {
            await SendEmailAsync(email, "Account rejected", UserRejected());
        }
        public async Task SendEmailAsync(string email, string subject, string body)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(_smtpSettings.SenderName, _smtpSettings.SenderEmail));

                message.To.Add(new MailboxAddress(email));
                message.Subject = subject;
                message.Body = new TextPart("html")
                {
                    Text = body
                };

                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.CheckCertificateRevocation = false;

                    if (_env.IsDevelopment())
                    {
                        await client.ConnectAsync(_smtpSettings.Server, _smtpSettings.Port, MailKit.Security.SecureSocketOptions.Auto);
                    }
                    else
                    {
                        await client.ConnectAsync(_smtpSettings.Server);
                    }

                    await client.AuthenticateAsync(_smtpSettings.Username, _smtpSettings.Password);
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }
        private string EmailConfirmed() { return "Congratulations!!! Your e-mail has been confirmed :)"; }
        private string ReportAproved(string reportName) { return $"Your report {reportName} has been approved and uploaded on our site :)"; }
        private string UserAproved() { return "Your request to register to our site has been approved :)"; }
        private string ReportRejected(string reportName) { return $"Your report {reportName} has been rejected :( {Environment.NewLine}" +
                $"Consider reworking it and try again"; }
        private string UserRejected() { return "Your request to register to our site has been rejected :("; }
    }
}