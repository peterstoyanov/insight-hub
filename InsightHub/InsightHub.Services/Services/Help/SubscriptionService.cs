﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.Mappers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Services.Help
{
    /// <summary>
    /// Contains subscription logics 
    /// </summary>
    public class SubscriptionService : ISubscriptionService
    {
        private readonly InsightHubContext context;
        private readonly IDTOMapper<Industry, IndustryDTO> dtoIndustryMapper;
        private readonly IDTOMapper<User, UserDTO> dtoUserMapper;
        private readonly IUserService userService;
        private readonly IIndustryService industryService;
        private readonly IMailer mailer;
        public SubscriptionService(InsightHubContext context,
            IDTOMapper<Industry, IndustryDTO> dtoIndustryMapper,
            IDTOMapper<User, UserDTO> dtoUserMapper,
            IUserService userService, IIndustryService industryService, IMailer mailer)
        {
            this.context = context;
            this.dtoIndustryMapper = dtoIndustryMapper;
            this.dtoUserMapper = dtoUserMapper;
            this.userService = userService;
            this.industryService = industryService;
            this.mailer = mailer;
        }
        public async Task<bool> HardDeleteSubscriptionAsync(int? userId, int? industryId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            if (industryId == null)
            {
                throw new ArgumentNullException("Industry id argument was null");
            }
            var sub = await context.Subscriptions.FirstOrDefaultAsync(x => x.UserId == userId
                && x.IndustryId == industryId);
            if (sub == null)
            {
                return false;
            }
            _ = context.Subscriptions.Remove(sub);
            _ = await context.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<IndustryDTO>> GetSubscriptionsOfUserAsync(int? userId)
        {
            return dtoIndustryMapper.MapFrom(await this.context.Subscriptions.Where(x => x.UserId == userId)
              .Select(x => x.Industry)
              .ToArrayAsync());
        }

        public async Task<IEnumerable<IndustryDTO>> GetUserNonSubscriptionsAsync(int? userId)
        {
            var subedInsdustries = await GetSubscriptionsOfUserAsync(userId);
            var nonSubedInstries = new HashSet<IndustryDTO>();
            var industries = (await this.industryService.GetAllIndustries());

            foreach (var item in industries)
            {
                if (subedInsdustries.FirstOrDefault(x => x.Id == item.Id) == null)
                {
                    nonSubedInstries.Add(item);
                }
            }

            return nonSubedInstries;
        }
        public async Task NotifySubscribers(int? industryId, ReportDTO report)
        {
            if (industryId == null)
            {
                throw new ArgumentNullException("Industry id argument was null");
            }
            if (report == null)
            {
                throw new ArgumentNullException("Report argument was null");
            }
            var subject = report.Name + " has been uploaded";
            var body = report.Description;
            var subs = await GetSubscribtions();
            foreach (var sub in subs)
            {
                await this.mailer.SendEmailAsync(sub.User.Email, subject, body);
            }
        }

        public async Task SubscribeAsync(int? userId, int? industryId)
        {
            var sub = await context.Subscriptions.FirstOrDefaultAsync(x => x.UserId == userId
                && x.IndustryId == industryId);
            if (sub == null)
            {
                sub = new Subscription
                {
                    UserId = (int)userId,
                    IndustryId = (int)industryId
                };
                context.Subscriptions.Add(sub);
            }

            await context.SaveChangesAsync();
        }

        async Task<bool> ISubscriptionService.DeleteSubscriptionAsync(int? userId, int? industryId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            if (industryId == null)
            {
                throw new ArgumentNullException("Industry id argument was null");
            }
            var sub = await context.Subscriptions.FirstOrDefaultAsync(x => x.UserId == userId
                && x.IndustryId == industryId);
            if (sub == null)
            {
                return false;
            }
            sub.IsDeleted = true;
            await context.SaveChangesAsync();
            return true;
        }
        private async Task<IEnumerable<Subscription>> GetSubscribtions()
        {
            return await context.Subscriptions
                .Include(x => x.User)
                .Include(x => x.Industry)
                .ToArrayAsync();
        }
    }
}
