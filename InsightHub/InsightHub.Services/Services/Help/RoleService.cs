﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Services.Help
{
    public class RoleService : IRoleService
    {
        private readonly InsightHubContext context;
        private readonly int AdminRole_Id = 1;
        private readonly int AuthorRole_Id = 2;
        private readonly int Customer_Id = 3;
        public RoleService(InsightHubContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task AssignRoleAsync(string roleName, int userId)
        {
            if (null != context.UserRoles.FirstOrDefaultAsync(x => x.UserId == userId))
            {
                int roleId = Customer_Id;
                if (roleName.ToLower() == "author")
                {
                    roleId = AuthorRole_Id;
                }
                var role = new IdentityUserRole<int>
                {
                    RoleId = roleId,
                    UserId = userId
                };
                await context.UserRoles.AddAsync(role);
                await context.SaveChangesAsync();
            }
        }
    }
}
