﻿using InsightHub.Services.Mappers.Contracts;
using InsightHub.Data.DataAccessContext;
using Microsoft.EntityFrameworkCore;
using InsightHub.Services.Contracts;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Data.Entities;
using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore.Query;

namespace InsightHub.Services.Services.Main
{
    /// <summary>
    /// Contains logics required for working with reports 
    /// </summary>
    public class ReportService : IReportService
    {
        private readonly InsightHubContext context;
        private readonly IDTOMapper<Report, ReportDTO> dtoMapper;
        private readonly IBlobService blobService;
        private readonly IReportTagsService reportTagsService;
        private readonly ISubscriptionService subscriptionService;
        private readonly IMailer mailer;
        public ReportService(InsightHubContext context, IDTOMapper<Report, ReportDTO> dtoMapper,
            IBlobService blobService, IMailer mailer, IReportTagsService reportTagsService, ISubscriptionService subscriptionService)
        {
            this.mailer = mailer ?? throw new ArgumentNullException(nameof(mailer));
            this.subscriptionService = subscriptionService ?? throw new ArgumentNullException(nameof(subscriptionService));
            this.blobService = blobService ?? throw new ArgumentNullException(nameof(blobService));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dtoMapper = dtoMapper ?? throw new ArgumentNullException(nameof(dtoMapper));
            this.reportTagsService = reportTagsService ?? throw new ArgumentNullException(nameof(reportTagsService));
        }

        public async Task<ReportDTO> AproveReportAsync(int? id)
        {
            var report = await GetReportEntityAsync(id);
            if (report.IsPending)
            {
                report.IsPending = false;
                report.UploadedOn = DateTime.UtcNow;
                var reportDTO = dtoMapper.MapFrom(report);
                await context.SaveChangesAsync();
                await blobService.UploadReportAsync(reportDTO);
                await mailer.ReportAproved(report.Author.Email, report.Name);
                await subscriptionService.NotifySubscribers(reportDTO.IndustryId, reportDTO);
                return reportDTO;
            }
            return dtoMapper.MapFrom(report);
        }
        public async Task RejectReportAsync(int? id)
        {
            var report = await GetReportEntityAsync(id);
            await mailer.ReportRejected(report.Author.Email, report.Name);
            report.IsDeleted = true;
            await context.SaveChangesAsync();
        }
        public async Task<ReportDTO> DownloadReportAsync(int? downloaderId, int? reportId)
        {
            if (downloaderId == null)
            {
                throw new ArgumentNullException("Downloader id argument was null");
            }
            if (reportId == null)
            {
                throw new ArgumentNullException("The report id argument was null");
            }
            var reportDownloader = await context.ReportDownloaders
                .FirstOrDefaultAsync(x => x.ReportId == reportId
                    && x.DownloaderId == downloaderId);
            if (reportDownloader == null)
            {
                reportDownloader = new ReportDownloaders()
                {
                    DownloaderId = (int)downloaderId,
                    ReportId = (int)reportId,
                    TimesDownloaded = 1,
                };
                _ = context.ReportDownloaders.AddAsync(reportDownloader);
            }
            else
            {
                reportDownloader.TimesDownloaded++;
            }
            await context.SaveChangesAsync();

            return await GetReportAsync(reportId);
        }
        public async Task<IEnumerable<ReportDTO>> GetDownloadedReportsByUserAsync(int? downloaderId)
        {
            if (downloaderId == null)
            {
                throw new ArgumentNullException("Downloader id argument was not null");
            }
            var reports = await GetReportsAsync().ToArrayAsync();

            var reportDTOs = new HashSet<ReportDTO>();
            foreach (var report in reports)
            {
                foreach (var downloader in report.ReportDownloaders)
                {
                    if (downloader.DownloaderId == downloaderId)
                    {
                        reportDTOs.Add(dtoMapper.MapFrom(report));
                    }
                }
            }
            return reportDTOs;
        }
        public async Task<IEnumerable<ReportDTO>> GetFeaturedReportsAsync(int count = 3)
        {
            var reports = (await GetAprovedReportsAsync());
            return reports.OrderByDescending(x => x.IsFeatured.ToString()).Take(count);
        }
        public async Task<IEnumerable<ReportDTO>> GetLatestReportsAsync(int count = 7)
        {
            var reports = await GetAprovedReportsAsync();
            return reports.TakeLast(count);
        }
        public async Task<IEnumerable<ReportDTO>> GetTopReportsAsync(int count = 7)
        {
            var reports = await GetAprovedReportsAsync();
            return reports.OrderByDescending(x => x.DownloadsCnt).Take(count);
        }
        public async Task<ReportDTO> FeatureReportAsync(int? id)
        {
            var report = await GetReportEntityAsync(id);
            report.IsFeatured = true;
            context.SaveChanges();
            return dtoMapper.MapFrom(report);
        }
        public async Task<ReportDTO> StopFeaturingReportAsync(int? id)
        {
            var report = await GetReportEntityAsync(id);
            report.IsFeatured = false;
            await context.SaveChangesAsync();
            return dtoMapper.MapFrom(report);
        }
        public async Task<IEnumerable<ReportDTO>> GetAprovedReportsAsync()
        {
            var reports = await GetReportsAsync()
                .Where(x => x.IsDeleted == false && x.IsPending == false)
                .ToArrayAsync();

            var mappedReports = this.dtoMapper.MapFrom(reports);
            return mappedReports;
        }
        public async Task<IEnumerable<ReportDTO>> GetPendingReportsAsync()
        {

            var reports = await GetReportsAsync()
             .Where(x => x.IsDeleted == false && x.IsPending == true)
             .ToArrayAsync();

            var mappedReports = this.dtoMapper.MapFrom(reports);
            return mappedReports;
        }
        public async Task<IEnumerable<ReportDTO>> GetReportsOfUserAsync(int? userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("User id argument was not null");
            }
            var reports = await GetReportsAsync()
             .Where(x => x.IsDeleted == false && x.AuthorId == userId)
             .ToArrayAsync();

            var mappedReports = this.dtoMapper.MapFrom(reports);
            return mappedReports;
        }
        public async Task<ReportDTO> CreateReportAsync(ReportDTO reportDTO)
        {
            if (reportDTO == null)
            {
                throw new ArgumentNullException("The report argument was not null");
            }
            var report = await context.Reports
                .FirstOrDefaultAsync(x => x.Name == reportDTO.Name
                    && x.AuthorId == reportDTO.AuthorId);
            if (report == null)
            {
                report = new Report();
                CopyInfo(report, reportDTO);
                await context.Reports.AddAsync(report);
            }
            else if (report.IsDeleted)
            {
                CopyInfo(report, reportDTO);
                report.IsDeleted = false;
            }
            else if (report != null)
            {
                throw new ArgumentException("You already have a report with this name!");
            }
            report.IsPending = true;
            await context.SaveChangesAsync();
            report = await GetReportsAsync()
                .FirstOrDefaultAsync(x => x.Name == reportDTO.Name
                    && x.AuthorId == reportDTO.AuthorId);
            reportDTO.Id = report.Id;
            await reportTagsService.AssignTags(reportDTO);
            reportDTO = this.dtoMapper.MapFrom(report);
            return reportDTO;
        }
        public async Task<ReportDTO> UpdateReportAsync(ReportDTO reportDTO)
        {
            if (reportDTO == null)
            {
                throw new ArgumentNullException("The report argument was not null");
            }
            var report = await context.Reports
                .FirstOrDefaultAsync(x => x.Id == reportDTO.Id);
            if (report == null)
            {
                throw new ArgumentException("The report was not found");
            }
            else if (report.IsDeleted)
            {
                throw new ArgumentException("The report is deleted");
            }
            else
            {
                CopyInfo(report, reportDTO);
            }
            report.IsPending = true;
            await context.SaveChangesAsync();
            await reportTagsService.AssignTags(reportDTO);
            report = await GetReportsAsync()
               .FirstOrDefaultAsync(x => x.Name == reportDTO.Name);
            reportDTO = this.dtoMapper.MapFrom(report);
            return reportDTO;
        }

        public async Task<ReportDTO> GetApprovedReportAsync(int? id)
        {
            var report = await GetReportEntityAsync(id);
            if (report.IsPending)
            {
                throw new ArgumentNullException("Report is not approved yet!");
            }
            var mappedReports = this.dtoMapper.MapFrom(report);
            return mappedReports;
        }
        public async Task<ReportDTO> GetPendingReportAsync(int? id)
        {
            var report = await GetReportEntityAsync(id);
            if (!report.IsPending)
            {
                throw new ArgumentNullException("Pending report not found!");
            }
            var mappedReports = this.dtoMapper.MapFrom(report);
            return mappedReports;
        }
        public async Task<IEnumerable<ReportDTO>> GetAllReportsAsync()
        {
            var reports = await GetReportsAsync().ToArrayAsync();

            var mappedReports = this.dtoMapper.MapFrom(reports);
            return mappedReports;
        }
        public async Task<bool> DeleteReportAsync(int? id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("Report id was null!");
            }
            var report = await this.context.Reports
                .FirstOrDefaultAsync(x => x.Id == id);

            if (report == null)
            {
                return false;
            }

            report.IsDeleted = true;
            report.DeletedOn = DateTime.UtcNow;
            await this.context.SaveChangesAsync();

            return report.IsDeleted;
        }
        public async Task<ReportDTO> GetReportAsync(int? id)
        {
            var report = await GetReportEntityAsync(id);

            var mappedReports = this.dtoMapper.MapFrom(report);
            return mappedReports;
        }
        internal async Task<Report> GetReportEntityAsync(int? reportId)
        {
            if (reportId == null)
            {
                throw new ArgumentNullException("The report id argument was not null");
            }
            var report = await context.Reports
               .Include(x => x.Industry)
               .Include(x => x.Author)
               .Include(x => x.ReportTags).ThenInclude(x => x.Tag)
               .Include(x => x.ReportDownloaders).ThenInclude(x => x.Downloader)
               .FirstOrDefaultAsync(x => x.Id == reportId);

            if (report == null)
            {
                throw new ArgumentException("The report was not found");
            }
            if (report.IsDeleted)
            {
                throw new ArgumentException("The report has been deleted");
            }
            return report;
        }
        private void CopyInfo(Report report, ReportDTO reportDTO)
        {
            report.Name = reportDTO.Name;
            report.Description = reportDTO.Description;
            report.Summary = reportDTO.Summary;
            report.IndustryId = reportDTO.IndustryId;
            report.BinaryContent = reportDTO.BinaryContent;
            report.UploadedOn = reportDTO.UploadedOn;
            report.IsPending = reportDTO.IsPending;
        }
        private IIncludableQueryable<Report, User> GetReportsAsync()
        {
            return this.context.Reports
               .Include(x => x.Industry)
               .Include(x => x.Author)
               .Include(x => x.ReportTags).ThenInclude(x => x.Tag)
               .Include(x => x.ReportDownloaders).ThenInclude(x => x.Downloader);
        }

    }
}
