﻿using Spire.Pdf.Graphics;
using System.Drawing;
using Spire.Pdf;
using System;

namespace InsightHub.Services.PDFServices
{
    /// <summary>
    /// This class is used to add custom layout to our pdf report docs
    /// </summary>
    public static class PDFDesign
    {
        public static void Layout(string content, string fileName, PdfDocument pdfDocument)
        {
            //The design part of the report
            //---------------- Header ------------------

            //reset the default margins to 0
            pdfDocument.PageSettings.Size = PdfPageSize.A4;
            pdfDocument.PageSettings.Margins = new PdfMargins(0);

            //create a PdfMargins object, the parameters indicate the page margins you want to set
            PdfMargins margins = new PdfMargins(60, 60, 60, 60);

            //create a header template with content and apply it to page template
            pdfDocument.Template.Top = CreateHeader(pdfDocument, margins);

            //apply blank templates to other parts of page template
            pdfDocument.Template.Bottom = new PdfPageTemplateElement(pdfDocument.PageSettings.Size.Width, margins.Bottom);
            pdfDocument.Template.Left = new PdfPageTemplateElement(margins.Left, pdfDocument.PageSettings.Size.Height);
            pdfDocument.Template.Right = new PdfPageTemplateElement(margins.Right, pdfDocument.PageSettings.Size.Height);

            //---------------- Report text ------------------

            PdfSection section = pdfDocument.Sections.Add();
            PdfPageBase page = section.Pages.Add();
            PdfFont font = new PdfFont(PdfFontFamily.Helvetica, 11);
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Center);

            format.LineSpacing = 25f;
            PdfBrush brush = PdfBrushes.Black;
            PdfTextWidget textWidget = new PdfTextWidget(content, font, brush);
            float y = 0;
            PdfTextLayout textLayout = new PdfTextLayout();
            textLayout.Break = PdfLayoutBreakType.FitPage;
            textLayout.Layout = PdfLayoutType.Paginate;
            RectangleF bounds = new RectangleF(new PointF(0, y), page.Canvas.ClientSize);
            textWidget.StringFormat = format;
            textWidget.Draw(page, bounds, textLayout);

            //create a footer template with content and apply it to page template 
            pdfDocument.Template.Bottom = CreateFooter(pdfDocument, margins);

        }

        public static PdfPageTemplateElement CreateHeader(PdfDocument doc, PdfMargins margins)
        {
            //get page size
            SizeF pageSize = doc.PageSettings.Size;

            //create a PdfPageTemplateElement object as header space
            PdfPageTemplateElement headerSpace = new PdfPageTemplateElement(pageSize.Width, margins.Top);
            headerSpace.Foreground = false;

            //declare two float variables
            float x = margins.Left;
            float y = 0;

            //draw image in header space 
            PdfImage headerImage = PdfImage.FromFile("logo.png");
            float width = headerImage.Width / 7;
            float height = headerImage.Height / 7;
            headerSpace.Graphics.DrawImage(headerImage, x, margins.Top - height - 4, width, height);

            //draw line in header space
            PdfPen pen = new PdfPen(PdfBrushes.Gray, 1);
            headerSpace.Graphics.DrawLine(pen, x, y + margins.Top - 2, pageSize.Width - x, y + margins.Top - 2);

            //draw text in header space
            PdfTrueTypeFont font = new PdfTrueTypeFont(new Font("Impact", 25f, FontStyle.Bold));
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Left);
            String headerText = "REPORT";
            SizeF size = font.MeasureString(headerText, format);
            headerSpace.Graphics.DrawString(headerText, font, PdfBrushes.Gray, pageSize.Width - x - size.Width - 2, margins.Top - (size.Height + 5), format);

            return headerSpace;
        }

        public static PdfPageTemplateElement CreateFooter(PdfDocument doc, PdfMargins margins)
        {
            //get page size
            SizeF pageSize = doc.PageSettings.Size;

            //create a PdfPageTemplateElement object which works as footer space
            PdfPageTemplateElement footerSpace = new PdfPageTemplateElement(pageSize.Width, margins.Bottom);
            footerSpace.Foreground = false;

            //declare two float variables
            float x = margins.Left;
            float y = 0;

            //draw line in footer space
            PdfPen pen = new PdfPen(PdfBrushes.Gray, 1);
            footerSpace.Graphics.DrawLine(pen, x, y, pageSize.Width - x, y);

            //draw text in footer space
            y = y + 5;
            PdfTrueTypeFont font = new PdfTrueTypeFont(new Font("Impact", 10f), true);
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Left);
            String footerText = "StackTracers Technology Co., Ltd.\nTel:0898 359 357\nWebsite:https://gitlab.com/peterstoyanov/insight-hub";
            footerSpace.Graphics.DrawString(footerText, font, PdfBrushes.Gray, x, y, format);

            return footerSpace;
        }
    }
}
