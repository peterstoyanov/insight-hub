﻿namespace InsightHub.Services.PDFServices.Contracts
{
    public interface IPDFConverter
    {
        /// <summary>
        /// Saves the content as pdf 
        /// </summary>
        void SaveStringAsPDF(string content, out string filePath, string fileName);        
    }
}
