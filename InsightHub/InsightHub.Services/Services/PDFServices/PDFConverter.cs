﻿using InsightHub.Services.PDFServices.Contracts;
using Spire.Pdf;
using System.IO;

namespace InsightHub.Services.PDFServices
{
    /// <summary>
    /// A class that is used to convert from txt content to pdf
    /// </summary>
    public class PDFConverter : IPDFConverter
    {
        public void SaveStringAsPDF(string content, out string filePath, string fileName)
        {
            string nameOfPdf = $"{fileName}";
            filePath = $"{Directory.GetCurrentDirectory()}\\PDF source\\{nameOfPdf}";

            PdfDocument doc = new PdfDocument();
            PDFDesign.Layout(content, fileName, doc);

            //Save PDF to file
            doc.SaveToFile(filePath, FileFormat.PDF);
        }
    }
}
