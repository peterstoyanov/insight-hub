﻿using InsightHub.Services.PDFServices.Contracts;
using InsightHub.Data.Entities.Blob;
using InsightHub.Services.Contracts;
using Azure.Storage.Blobs.Models;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using System.Text;
using System.IO;
using System;

namespace InsightHub.Services.Services.Blob
{
    /// <summary>
    /// Contains logic for working with the blob storage
    /// </summary>
    public class BlobService : IBlobService
    {
        private readonly BlobServiceClient blobServiceClient;
        private readonly IPDFConverter pDFConverter;

        public BlobService(BlobServiceClient blobServiceClient, IPDFConverter pDFConverter)
        {
            this.blobServiceClient = blobServiceClient;
            this.pDFConverter = pDFConverter;
        }
        public async Task UploadReportAsync(ReportDTO reportDTO)
        {
            string content = Environment.NewLine + reportDTO.ReportFormat();
            string pathPDF;
            string nameOfPDF = reportDTO.Name + ".pdf";
            this.pDFConverter.SaveStringAsPDF(content, out pathPDF, nameOfPDF);
            await UploadFileBlobAsync(pathPDF, nameOfPDF);
        }
        public async Task<BlobInformation> GetBlobAsync(string name)
        {
            var containerClient = blobServiceClient.GetBlobContainerClient("reports");
            var blobClient = containerClient.GetBlobClient(name);
            var blobDownloadInfo = await blobClient.DownloadAsync();
            return new BlobInformation(blobDownloadInfo.Value.Content, blobDownloadInfo.Value.ContentType);
        }

        public async Task<IEnumerable<string>> ListBlobsAsync()
        {
            var containerClient = blobServiceClient.GetBlobContainerClient("reports");
            var items = new List<string>();

            await foreach (var blobItem in containerClient.GetBlobsAsync())
            {
                items.Add(blobItem.Name);
            }

            return items;
        }

        private async Task UploadFileBlobAsync(string filePath, string fileName)
        {
            var containerClient = blobServiceClient.GetBlobContainerClient("reports");
            var blobClient = containerClient.GetBlobClient(fileName);
            await blobClient.UploadAsync(filePath, new BlobHttpHeaders { ContentType = filePath.GetContentType() });
        }

        public async Task UploadContentBlobAsync(string content, string fileName)
        {
            var containerClient = blobServiceClient.GetBlobContainerClient("reports");
            var blobClient = containerClient.GetBlobClient(fileName);
            var bytes = Encoding.UTF8.GetBytes(content);
            await using var memoryStream = new MemoryStream(bytes);
            await blobClient.UploadAsync(memoryStream, new BlobHttpHeaders { ContentType = fileName.GetContentType() });
        }

        public async Task DeleteBlobAsync(string blobName)
        {
            var containerClient = blobServiceClient.GetBlobContainerClient("reports");
            var blobClient = containerClient.GetBlobClient(blobName);
            await blobClient.DeleteIfExistsAsync();
        }
    }
}

