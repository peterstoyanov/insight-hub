﻿using System.Collections.Generic;
using System.Text;
using System;

namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A model class that transfers Report concerning data from the business services 
    /// to the potential consuming assemblies and vice versa
    /// </summary>
    public class ReportDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
        public int IndustryId { get; set; }
        public string IndustryImg { get; set; }
        public string IndustryName { get; set; }
        public IEnumerable<string> TagsIds { get; set; }
        public IEnumerable<string> TagNames { get; set; }
        public IEnumerable<string> NamesOfDownloaders { get; set; }
        public string BinaryContent { get; set; }
        public int DownloadsCnt { get; set; }
        public DateTime UploadedOn { get; set; }
        public bool IsPending { get; set; }
        public bool IsFeatured { get; set; }
        public override string ToString()
        {
            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.AppendLine($"Id: {this.Id}");
            contentBuilder.AppendLine($"Name: {this.Name}");
            contentBuilder.AppendLine($"Author name: {this.AuthorName}");
            contentBuilder.AppendLine($"was downloaded: {this.DownloadsCnt} times");
            contentBuilder.AppendLine($"Industry: {this.IndustryName}");
            contentBuilder.AppendLine($"Uploaded on: {this.UploadedOn}");
            contentBuilder.AppendLine($"Description:");
            contentBuilder.AppendLine($"Tags: {string.Join(", ", this.TagNames)}");
            contentBuilder.AppendLine(this.Description);
            return contentBuilder.ToString();
        }

        public string ReportFormat()
        {
            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.AppendLine($"Author name {this.AuthorName}");
            contentBuilder.AppendLine($"was downloaded {this.DownloadsCnt} times");
            contentBuilder.AppendLine($"Summary");
            contentBuilder.AppendLine($"{this.Summary}");
            contentBuilder.AppendLine($"Description");
            contentBuilder.AppendLine(this.Description);
            return contentBuilder.ToString();
        }
    }
}