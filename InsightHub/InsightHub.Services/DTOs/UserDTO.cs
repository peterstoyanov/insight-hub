﻿using System.Text.Json.Serialization;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using System.Text;

namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A model class that transfers User concerning data from the business services 
    /// to the potential consuming assemblies and vice versa
    /// </summary>
    public class UserDTO
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsApprovedByAdmin { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        //[JsonIgnore]
        public string Password { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsBanned { get; set; }
        public IEnumerable<Ban> Bans { get; set; }
        public IEnumerable<string> NamesOfWrittenReports { get; set; }
        public IEnumerable<string> NamesOfIndustries { get; set; }
        public IEnumerable<string> NamesOfDownloadedReports { get; set; }
        public IEnumerable<string> NamesOfSubscriptions { get; set; }
        public override string ToString()
        {
            {
                StringBuilder contentBuilder = new StringBuilder();
                contentBuilder.AppendLine($"Id: {this.Id}");
                contentBuilder.AppendLine($"Name: {this.Name}");
                return contentBuilder.ToString();
            }
        }
    }
}
