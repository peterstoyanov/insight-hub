﻿using InsightHub.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface ISubscriptionService
    {
        /// <summary>
        /// Soft deletes a subscription from the subscription list of the user
        /// </summary>
        public Task<bool> DeleteSubscriptionAsync(int? userId, int? industryId);//useful in case we would be interested in the subscription history
        /// <summary>
        /// Hard deletes a subscription from the subscription list of the user
        /// </summary>
        public Task<bool> HardDeleteSubscriptionAsync(int? userId, int? industryId);
        /// <summary>
        /// Notifies that a report has been uploaded
        /// </summary>
        public Task NotifySubscribers(int? industryId, ReportDTO report);
        /// <summary>
        /// Subscribes the user (the user will be notified when an report from the given industry has been uploaded)
        /// </summary>
        public Task SubscribeAsync(int? userId, int? industryId);
        /// <summary>
        /// Retrieves the user's subscribtion list (the industries he/she has subcribed for)
        /// </summary>
        public Task<IEnumerable<IndustryDTO>> GetSubscriptionsOfUserAsync(int? userId);
        /// <summary>
        /// Retrieves the all industries for which the user is not subscribed
        /// </summary>
        public Task<IEnumerable<IndustryDTO>> GetUserNonSubscriptionsAsync(int? userId);
    }
}
