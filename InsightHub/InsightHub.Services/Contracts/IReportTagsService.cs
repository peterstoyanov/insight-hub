﻿using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IReportTagsService
    {
        /// <summary>
        /// Assigns tags to the report (the ids of the tags must be loaded in the reportDTO's TagsIds property)
        /// </summary>
        Task AssignTags(ReportDTO reportDTO);
    }
}