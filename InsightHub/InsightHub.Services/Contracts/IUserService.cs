﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IUserService
    {
        /// <summary>
        /// Creates a user (similar to registering)
        /// </summary>
        Task<UserDTO> CreateUserAsync(UserDTO userDTO);
        /// <summary>
        /// Soft deletes a user 
        /// </summary>
        Task<bool> DeleteUserAsync(int? id);
        /// <summary>
        /// Retrieves the users from the database 
        /// </summary>
        Task<IEnumerable<UserDTO>> GetAllValidUsersAsync();
        /// <summary>
        /// Retrieves a users from the database 
        /// </summary>
        Task<UserDTO> GetUserAsync(int? id);
        /// <summary>
        /// Retrieves a users from the database 
        /// </summary>
        Task<UserDTO> UpdateUserAsync(UserDTO userDTO);
        /// <summary>
        /// Sets a user's status to approved
        /// </summary>
        Task<UserDTO> AproveUserAsync(int? id);
        /// <summary>
        /// Retrieves the approved users from the database 
        /// </summary>
        Task<IEnumerable<UserDTO>> GetAprovedUsersAsync();
        /// <summary>
        /// Retrieves the users with pending status from the database 
        /// </summary>
        Task<IEnumerable<UserDTO>> GetPendingUsersAsync();
        Task<UserDTO> GetUserByTokenAsync(string token);
    }
}