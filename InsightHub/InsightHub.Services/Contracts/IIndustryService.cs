﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IIndustryService
    {
        /// <summary>
        /// Retrieves the industries from the database 
        /// </summary>
        /// <returns>The industries as DTOs</returns>
        Task<IEnumerable<IndustryDTO>> GetAllIndustries();
        /// <summary>
        /// Retrieves a industry from the database 
        /// </summary>
        /// <returns>The industries of the user as DTOs</returns>
        Task<IndustryDTO> GetIndusry(int? id);
    }
}