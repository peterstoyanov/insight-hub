using Azure.Storage.Blobs;
using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using InsightHub.Services.Contracts;
using InsightHub.Services.PDFServices;
using InsightHub.Services.Services.Blob;
using InsightHub.Services.Services.Help;
using InsightHub.Services.Services.Mail;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace InsightHub.Test
{
    public static class InsightHubUtils
    {
        public static int IT_Id = 1;
        public static int HealthCare_Id = 2;
        public static int Finance_Id = 3;
        public static int Defence_Id = 4;
        public static int Gaming_Id = 5;
        public static int AdminRole_Id = 1;
        public static int AuthorRole_Id = 2;
        public static int Customer_Id = 3;
        public static int WhatIsNewInJava8_Id = 1;
        public static int WC3IsBeingSavedFromPad_Id = 2;
        public static int HowGoodIsAO2DE_Id = 3;
        public static int HowWillWePreventCrisis_Id = 4;
        public static int HowImportantIsVitaminA_Id = 5;
        public static int HowImportantIsVitaminC_Id = 6;
        public static int HowImportantIsVitaminD_Id = 7;
        public static int Yani_Id = 1;
        public static int Pesho_Id = 2;
        public static int Uther_Id = 3;
        public static int Baal_Id = 4;
        public static int Mephisto_Id = 5;
        public static int Uther2_Id = 6;
        public static int B2W_Id = 1;
        public static int D2_Id = 2;
        public static int RVD_Id = 3;
        public static IBlobService blobService = new BlobService(new BlobServiceClient(""), new PDFConverter());
        //static IOptions<SmtpSettings> smtpSettings;
        //static IWebHostEnvironment env;
        public static IMailer mailer = new Mailer(null, null);
        public static DbContextOptions<InsightHubContext> GetOptions()
        {
            var nameOfDb = Guid.NewGuid().ToString();
            return new DbContextOptionsBuilder<InsightHubContext>()
                .UseInMemoryDatabase(databaseName: nameOfDb)
                .Options;
        }
        public static IReportTagsService reportTagsService = new ReportTagsService(new InsightHubContext(GetOptions()));  
        public static void Seed(InsightHubContext context)
        {
            var industries = new Industry[] {
                new Industry
                {
                    Id = IT_Id,
                    Name = "IT",
                },
                new Industry
                {
                    Id = HealthCare_Id,
                    Name = "HealthCare",
                },
                new Industry
                {
                    Id = Finance_Id,
                    Name = "Finance",
                },
                new Industry
                {
                    Id = Defence_Id,
                    Name = "Defence",
                    IsDeleted = true
                },
                new Industry
                {
                    Id = Gaming_Id,
                    Name = "Gaming",
                },
            };
            var roles = new Role[] {
                new Role
                {
                    Id = AdminRole_Id,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new Role
                {
                    Id = AuthorRole_Id,
                    Name = "Author",
                    NormalizedName = "AUTHOR"
                },
                new Role
                {
                    Id = Customer_Id,
                    Name = "Customer",
                    NormalizedName = "CUSTOMER"
                },
            };
            var users = new User[] {
                new User
                {
                    Id = Yani_Id,
                    Name = "Yani Yordanov",
                    Email = "qni_1999@mail.bg",
                    NormalizedEmail = "QNI_1999@MAIL.BG",
                    IsApprovedByAdmin = false
                },
                new User
                {
                    Id = Pesho_Id,
                    Name = "Peter Stoyanov",
                    Email = "pesho_1999@mail.bg",
                    NormalizedEmail = "PESHO_1999@MAIL.BG",
                    IsApprovedByAdmin = false
                },
                new User
                {
                    Id = Baal_Id,
                    Name = "Baal Stoyanov",
                    Email = "Baal_Id_1999@mail.bg",
                    NormalizedEmail = "BAAL_ID_1999@MAIL.BG",
                    IsApprovedByAdmin = false
                },
                new User
                {
                    Id = Mephisto_Id,
                    Name = "Mephisto Stoyanov",
                    Email = "Mephisto_1999@mail.bg",
                    NormalizedEmail = "MEPHISTO_1999@MAIL.BG",
                    IsApprovedByAdmin = true
                },
                new User
                {
                    Id = Uther_Id,
                    Name = "Pete Stoyanov",
                    Email = "pesh_1999@mail.bg",
                    NormalizedEmail = "PESH_1999@MAIL.BG",
                    LockoutEnd = DateTime.UtcNow.AddDays(2)
                },
                new User
                {
                    Id = Uther2_Id,
                    Name = "Uther2_Id Stoyanov",
                    Email = "Uther2_Id@mail.bg",
                    NormalizedEmail = "UT_1999@MAIL.BG",
                    IsDeleted = true,
                },
            };
            users[0].PasswordHash = new PasswordHasher<User>().HashPassword(users[0], "12345678");
            users[1].PasswordHash = new PasswordHasher<User>().HashPassword(users[1], "12345678");
            users[2].PasswordHash = new PasswordHasher<User>().HashPassword(users[2], "12345678");
            users[3].PasswordHash = new PasswordHasher<User>().HashPassword(users[3], "12345678");
            users[4].PasswordHash = new PasswordHasher<User>().HashPassword(users[4], "12345678");
            users[5].PasswordHash = new PasswordHasher<User>().HashPassword(users[5], "12345678");
            var tags = new Tag[]
            {
                new Tag
                {
                    Id = B2W_Id,
                    Name = "B2W"
                },
                new Tag
                {
                    Id = D2_Id,
                    Name = "D2"
                },
                new Tag
                {
                    Id = RVD_Id,
                    Name = "RVD"
                },
            };
            var reportTags = new ReportTags[]
            {
                new ReportTags
                {
                    TagId = B2W_Id,
                    ReportId = WC3IsBeingSavedFromPad_Id
                },
                 new ReportTags
                {
                    TagId = B2W_Id,
                    ReportId = HowGoodIsAO2DE_Id
                },
                new ReportTags
                {
                    TagId = D2_Id,
                    ReportId = HowGoodIsAO2DE_Id
                }
            };
            var reports = new Report[]
            {
                new Report
                {
                    Id = WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = Pesho_Id,
                    IndustryId = Gaming_Id,
                },
                new Report
                {
                    Id = WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = Yani_Id,
                    IndustryId = Gaming_Id,
                },
                new Report
                {
                    Id = HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = Yani_Id,
                    IndustryId = Gaming_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = Mephisto_Id,
                    IndustryId = HealthCare_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = Mephisto_Id,
                    IndustryId = HealthCare_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = HowImportantIsVitaminD_Id,
                    Name = "How important is vitamin D?",
                    Description = "How important is vitamin D?",
                    AuthorId = Mephisto_Id,
                    IndustryId = HealthCare_Id,
                    IsDeleted = true
                },
            };
           
            var repotDownloaders = new ReportDownloaders[]
                {
                    new ReportDownloaders
                    {
                        DownloaderId = Yani_Id,
                        ReportId = WC3IsBeingSavedFromPad_Id,
                        TimesDownloaded = 3
                    },
                     new ReportDownloaders
                    {
                        DownloaderId = Yani_Id,
                        ReportId = WhatIsNewInJava8_Id,
                        TimesDownloaded = 1
                    },
                };
            context.Industries.AddRange(industries);
            context.Roles.AddRange(roles);
            context.Users.AddRange(users);
            context.Reports.AddRange(reports);
            context.ReportDownloaders.AddRange(repotDownloaders);
            context.Tags.AddRange(tags);
            context.ReportTags.AddRange(reportTags);
            context.SaveChanges();

        }
    }
}
