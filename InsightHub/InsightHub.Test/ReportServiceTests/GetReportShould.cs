﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;
using InsightHub.Services.Services.Help;
using InsightHub.Services.Contracts;

namespace InsightHub.Test.ReportService
{
    [TestClass]
    public class GetReportShould
    {
        private class FakeSubService : ISubscriptionService
        {
            public Task<bool> DeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }

            public Task<IEnumerable<IndustryDTO>> GetSubscriptionsOfUserAsync(int? userId)
            {
                return default;
            }
            public Task<IEnumerable<IndustryDTO>> GetUserNonSubscriptionsAsync(int? userId)
            {
                return default;
            }
            public Task<bool> HardDeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }
            public Task NotifySubscribers(int? industryId, ReportDTO report)
            {
                return default;
            }
            public Task SubscribeAsync(int? userId, int? industryId)
            {
                return default;
            }
        }
        private class FakeMap : IDTOMapper<Report, ReportDTO>
        {
            public ReportDTO MapFrom(Report entity)
            {
                return new ReportDTO
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    AuthorId = entity.AuthorId,
                    IndustryId = entity.IndustryId,
                    IsPending = entity.IsPending
                };
            }

            public IEnumerable<ReportDTO> MapFrom(IEnumerable<Report> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task GetReportShould_Succeed()
        {

            var options = InsightHubUtils.GetOptions();
            Report expected = new Report
            {
                Id = InsightHubUtils.WhatIsNewInJava8_Id,
                Name = "What is new in Java 8?",
                Description = "What is new in Java 8?",
                AuthorId = InsightHubUtils.Pesho_Id,
                IndustryId = InsightHubUtils.Gaming_Id,
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());
                var result = (await sut.GetReportAsync(expected.Id));

                Assert.AreEqual(expected.Id, result.Id);
                Assert.AreEqual(expected.Name, result.Name);
                Assert.AreEqual(expected.Description, result.Description);
                Assert.AreEqual(expected.AuthorId, result.AuthorId);
                Assert.AreEqual(expected.IndustryId, result.IndustryId);
            }
        }
        [TestMethod]
        public async Task GetReportShould_Throw_DeletedReport()
        {

            var options = InsightHubUtils.GetOptions();
            Report deletedReport = new Report
            {
                Id = InsightHubUtils.HowImportantIsVitaminD_Id,
                Name = "How important is vitamin D?",
                Description = "How important is vitamin D?",
                AuthorId = InsightHubUtils.Mephisto_Id,
                IndustryId = InsightHubUtils.HealthCare_Id,
                IsDeleted = true
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer,
                    new ReportTagsService(context), new FakeSubService());
                await Assert.ThrowsExceptionAsync<ArgumentNullException>
                    (async () => await sut.GetReportAsync(deletedReport.Id));
            }
        }
        [TestMethod]
        public async Task GetReportShould_Throw_Missing()
        {

            var options = InsightHubUtils.GetOptions();
            Report wrongReport = new Report
            {
                Id = 90909,
                Name = "What is new in Java 8?",
                Description = "What is new in Java 8?",
                AuthorId = InsightHubUtils.Pesho_Id,
                IndustryId = InsightHubUtils.Gaming_Id,
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());

                await Assert.ThrowsExceptionAsync<ArgumentNullException>
                    (async () => await sut.GetReportAsync(wrongReport.Id));
            }
        }
    }
}
