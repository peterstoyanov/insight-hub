﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;
using InsightHub.Services.Services.Help;
using InsightHub.Services.Contracts;

namespace InsightHub.Test.ReportService
{
    [TestClass]
    public class GetDownloadedReportsByUserShould
    {
        private class FakeSubService : ISubscriptionService
        {
            public Task<bool> DeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }

            public Task<IEnumerable<IndustryDTO>> GetSubscriptionsOfUserAsync(int? userId)
            {
                return default;
            }
            public Task<IEnumerable<IndustryDTO>> GetUserNonSubscriptionsAsync(int? userId)
            {
                return default;
            }
            public Task<bool> HardDeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }
            public Task NotifySubscribers(int? industryId, ReportDTO report)
            {
                return default;
            }
            public Task SubscribeAsync(int? userId, int? industryId)
            {
                return default;
            }
        }
        private class FakeMap : IDTOMapper<Report, ReportDTO>
        {
            public ReportDTO MapFrom(Report entity)
            {
                return new ReportDTO
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    AuthorId = entity.AuthorId,
                    IndustryId = entity.IndustryId,
                    IsPending = entity.IsPending,
                };
            }
            public IEnumerable<ReportDTO> MapFrom(IEnumerable<Report> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task GetDownloadedReportsByUserShould_Succeed()
        {
            var options = InsightHubUtils.GetOptions();
            Report[] expected = new Report[]
            {
                new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());
                var result = (await sut.GetDownloadedReportsByUserAsync(InsightHubUtils.Yani_Id)).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
    }
}
