﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;
using InsightHub.Services.Services.Help;
using InsightHub.Services.Contracts;

namespace InsightHub.Test.ReportService
{
    [TestClass]
    public class DownloadReportShould
    {
        private class FakeSubService : ISubscriptionService
        {
            public Task<bool> DeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }

            public Task<IEnumerable<IndustryDTO>> GetSubscriptionsOfUserAsync(int? userId)
            {
                return default;
            }
            public Task<IEnumerable<IndustryDTO>> GetUserNonSubscriptionsAsync(int? userId)
            {
                return default;
            }
            public Task<bool> HardDeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }
            public Task NotifySubscribers(int? industryId, ReportDTO report)
            {
                return default;
            }
            public Task SubscribeAsync(int? userId, int? industryId)
            {
                return default;
            }
        }
        private class FakeMap : IDTOMapper<Report, ReportDTO>
        {
            public ReportDTO MapFrom(Report entity)
            {
                return new ReportDTO
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    AuthorId = entity.AuthorId,
                    IndustryId = entity.IndustryId,
                    IsPending = entity.IsPending,
                    DownloadsCnt = entity.ReportDownloaders.Select(x => x.TimesDownloaded).Sum(),
                    NamesOfDownloaders = entity.ReportDownloaders.Select(x => x.Downloader.Name)
                };
            }

            public IEnumerable<ReportDTO> MapFrom(IEnumerable<Report> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task DownloadReportShould_Succeed()
        {
            var options = InsightHubUtils.GetOptions();
            var expectedReport = new ReportDTO
            {
                Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                Name = "How important is vitamin A?",
                Description = "How important is vitamin A?",
                DownloadsCnt = 1,
                AuthorId = InsightHubUtils.Mephisto_Id,
                IndustryId = InsightHubUtils.HealthCare_Id,
                IsPending = true
            };
            var expectedUser = new User
            {
                Id = InsightHubUtils.Baal_Id,
                Name = "Baal Stoyanov",
                Email = "Baal_Id_1999@mail.bg",
                NormalizedEmail = "BAAL_ID_1999@MAIL.BG",
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer,
                    new ReportTagsService(context), new FakeSubService());
                var result = (await sut.DownloadReportAsync(expectedUser.Id, expectedReport.Id));


                Assert.AreEqual(expectedReport.Name, result.Name);
                Assert.AreEqual(expectedReport.Description, result.Description);
                Assert.AreEqual(expectedReport.AuthorId, result.AuthorId);
                Assert.AreEqual(expectedReport.DownloadsCnt, result.DownloadsCnt);
                Assert.AreEqual(expectedReport.IndustryId, result.IndustryId);
                Assert.AreEqual(expectedUser.Name, result.NamesOfDownloaders.First());
            }
        }
        [TestMethod]
        public async Task DownloadReportShould_Succeed_2ndDownloadByUser()
        {
            var options = InsightHubUtils.GetOptions();
            var expectedReport = new ReportDTO
            {
                Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                Name = "How important is vitamin A?",
                Description = "How important is vitamin A?",
                DownloadsCnt = 2,
                AuthorId = InsightHubUtils.Mephisto_Id,
                IndustryId = InsightHubUtils.HealthCare_Id,
                IsPending = true
            };
            var expectedUser = new User
            {
                Id = InsightHubUtils.Baal_Id,
                Name = "Baal Stoyanov",
                Email = "Baal_Id_1999@mail.bg",
                NormalizedEmail = "BAAL_ID_1999@MAIL.BG",
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());
                _ = await sut.DownloadReportAsync(expectedUser.Id, expectedReport.Id);
                var result = (await sut.DownloadReportAsync(expectedUser.Id, expectedReport.Id));

                Assert.AreEqual(expectedReport.Name, result.Name);
                Assert.AreEqual(expectedReport.Description, result.Description);
                Assert.AreEqual(expectedReport.AuthorId, result.AuthorId);
                Assert.AreEqual(expectedReport.DownloadsCnt, result.DownloadsCnt);
                Assert.AreEqual(expectedReport.IndustryId, result.IndustryId);
                Assert.AreEqual(expectedUser.Name, result.NamesOfDownloaders.First());
            }
        }
        [TestMethod]
        public async Task DownloadReportShould_Throw_NonexistingReport()
        {
            var options = InsightHubUtils.GetOptions();

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());
                await Assert.ThrowsExceptionAsync<ArgumentException>
                    (async () => await sut.DownloadReportAsync(908, 22));
            }
        }
        [TestMethod]
        public async Task DownloadReportShould_Throw_NonexistingUser()
        {
            var options = InsightHubUtils.GetOptions();

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer,
                    new ReportTagsService(context), new FakeSubService());
                await Assert.ThrowsExceptionAsync<ArgumentException>
                    (async () => await sut.DownloadReportAsync(InsightHubUtils.HowImportantIsVitaminA_Id, 22));
            }
        }
    }
}
