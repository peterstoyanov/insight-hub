﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;
using InsightHub.Services.Services.Help;
using Microsoft.EntityFrameworkCore;

namespace InsightHub.Test.ReportService
{
    [TestClass]
    public class SortReportsShould
    {
        private class FakeMap : IDTOMapper<Report, ReportDTO>
        {
            public ReportDTO MapFrom(Report entity)
            {
                return new ReportDTO
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    AuthorId = entity.AuthorId,
                    IndustryId = entity.IndustryId,
                    IndustryName = entity.Industry.Name,
                    TagNames = entity.ReportTags.Select(x => x.Tag.Name),
                    IsPending = entity.IsPending,
                    UploadedOn = entity.UploadedOn,
                    DownloadsCnt = entity.DownloadsCnt
                };
            }

            public IEnumerable<ReportDTO> MapFrom(IEnumerable<Report> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task SortReportsShould_Succeed_ByDefault()
        {

            var options = InsightHubUtils.GetOptions();
            Report[] expected = new Report[]
                {
                     new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    IsPending = true
                },
                      new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
               
               
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new SortReportsService(new Services.Services.Main.ReportService(context, new FakeMap(), 
                    InsightHubUtils.blobService, InsightHubUtils.mailer,
                    new ReportTagsService(context), null));
                var result = (await sut.SortReportsAsync("sadf")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
        [TestMethod]
        public async Task SortReportsShould_Succeed_ByName()
        {

            var options = InsightHubUtils.GetOptions();
            Report[] expected = new Report[]
                {
                     new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    IsPending = true
                },
                      new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },


            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new SortReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), null));
                var result = (await sut.SortReportsAsync("name")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }

        [TestMethod]
        public async Task SortReportsShould_Succeed_ByNameDesc()
        {

            var options = InsightHubUtils.GetOptions();
            Report[] expected = new Report[]
                {
                     new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    IsPending = true
                },
                      new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
            };
            expected = expected.OrderByDescending(x => x.Name).ToArray();
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new SortReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), null));
                var result = (await sut.SortReportsAsync("name_desc")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
        [TestMethod]
        public async Task SortReportsShould_Succeed_ByDownloads()
        {

            var options = InsightHubUtils.GetOptions();
            int cnt = 10;

            Report[] expected = new Report[]
                {
                    new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    DownloadsCnt = cnt*=10,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    DownloadsCnt = cnt*=10,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    DownloadsCnt = cnt*=10,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    DownloadsCnt = cnt*=10,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    DownloadsCnt = cnt*=10,
                    IsPending = true
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                cnt = 10;
                InsightHubUtils.Seed(context);
                await context.Reports.ForEachAsync(x => x.DownloadsCnt += (cnt*=10));
                context.SaveChanges();
            }
            expected = expected.OrderBy(x => x.DownloadsCnt).ToArray();
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new SortReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer,
                    new ReportTagsService(context), null));
                var result = (await sut.SortReportsAsync("download")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
        [TestMethod]
        public async Task SortReportsShould_Succeed_ByDownloadsDesc()
        {

            var options = InsightHubUtils.GetOptions();
            int cnt = 10;

            Report[] expected = new Report[]
                {
                    new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    DownloadsCnt = cnt*=10,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    DownloadsCnt = cnt*=10,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    DownloadsCnt = cnt*=10,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    DownloadsCnt = cnt*=10,
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    DownloadsCnt = cnt*=10,
                    IsPending = true
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                cnt = 10;
                InsightHubUtils.Seed(context);
                await context.Reports.ForEachAsync(x => x.DownloadsCnt += (cnt *= 10));
                context.SaveChanges();
            }
            expected = expected.OrderByDescending(x => x.DownloadsCnt).ToArray();
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new SortReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, new ReportTagsService(context), null));
                var result = (await sut.SortReportsAsync("download_desc")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }

        [TestMethod]
        public async Task SortReportsShould_Succeed_ByUploads()
        {

            var options = InsightHubUtils.GetOptions();
            int cnt = 1;

            Report[] expected = new Report[]
                {
                    new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IsPending = true
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                cnt = 1;
                InsightHubUtils.Seed(context);
                await context.Reports.ForEachAsync(x => x.UploadedOn = DateTime.UtcNow.AddDays(++cnt));
                context.SaveChanges();
            }
            expected = expected.OrderBy(x => x.UploadedOn).ToArray();
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new SortReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, new ReportTagsService(context), null));
                var result = (await sut.SortReportsAsync("uploadDate")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
        [TestMethod]
        public async Task SortReportsShould_Succeed_ByUploadsDesc()
        {

            var options = InsightHubUtils.GetOptions();
            int cnt = 1;

            Report[] expected = new Report[]
                {
                    new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Description = "How important is vitamin A?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IsPending = true
                },
                new Report
                {
                    Id = InsightHubUtils.HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Description = "How important is vitamin C?",
                    AuthorId = InsightHubUtils.Mephisto_Id,
                    IndustryId = InsightHubUtils.HealthCare_Id,
                    UploadedOn = DateTime.UtcNow.AddDays(++cnt),
                    IsPending = true
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                cnt = 1;
                InsightHubUtils.Seed(context);
                await context.Reports.ForEachAsync(x => x.UploadedOn = DateTime.UtcNow.AddDays(++cnt));
                context.SaveChanges();
            }
            expected = expected.OrderByDescending(x => x.UploadedOn).ToArray();
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new SortReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), null));
                var result = (await sut.SortReportsAsync("uploadDate_desc")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
    }
}
