﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Test.UserService
{
    [TestClass]
    public class DeleteUserShould
    {
        private class FakeMap : IDTOMapper<User, UserDTO>
        {
            public UserDTO MapFrom(User entity)
            {
                return new UserDTO
                {
                    IsDeleted = entity.IsDeleted,
                    IsBanned = entity.LockoutEnd > DateTime.UtcNow,
                    Id = entity.Id,
                    Name = entity.Name,
                    Email = entity.Email
                };
            }

            public IEnumerable<UserDTO> MapFrom(IEnumerable<User> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task DeleteUserShould_Throw_NullArgument()
        {
            var options = InsightHubUtils.GetOptions();

            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                await Assert.ThrowsExceptionAsync<ArgumentNullException>
                    (async () => await sut.DeleteUserAsync(null));
            }
        }
        [TestMethod]
        public async Task DeleteUserShould_Return_False()
        {
            var options = InsightHubUtils.GetOptions();
           
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                var result = (await sut.DeleteUserAsync(InsightHubUtils.Baal_Id));

                Assert.IsFalse(result);
            }
        }
        [TestMethod]
        public async Task DeleteUserShould_Return_True()
        {
            var options = InsightHubUtils.GetOptions();

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                var result = (await sut.DeleteUserAsync(InsightHubUtils.Baal_Id));

                Assert.IsTrue(result);
            }
        }
    }
}
