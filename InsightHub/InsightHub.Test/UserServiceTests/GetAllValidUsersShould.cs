﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Test.UserService
{
    [TestClass]
    public class GetAllValidUsersAsync
    {
        private class FakeMap : IDTOMapper<User, UserDTO>
        {
            public UserDTO MapFrom(User entity)
            {
                return new UserDTO
                {
                    IsDeleted = entity.IsDeleted,
                    IsBanned = entity.LockoutEnd > DateTime.UtcNow,
                    Id = entity.Id,
                    Name = entity.Name,
                    Email = entity.Email
                };
            }

            public IEnumerable<UserDTO> MapFrom(IEnumerable<User> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task GetAllValidUsersShould_Succeed()
        {

            var options = InsightHubUtils.GetOptions();
            User[] expected = new User[]
                {
               new User
                {
                    Id = InsightHubUtils.Yani_Id,
                    Name = "Yani Yordanov",
                    Email = "qni_1999@mail.bg",
                },
                new User
                {
                    Id = InsightHubUtils.Pesho_Id,
                    Name = "Peter Stoyanov",
                    Email = "pesho_1999@mail.bg",
                },
                new User
                {
                    Id = InsightHubUtils.Baal_Id,
                    Name = "Baal Stoyanov",
                    Email = "Baal_Id_1999@mail.bg",
                },
                new User
                {
                    Id = InsightHubUtils.Mephisto_Id,
                    Name = "Mephisto Stoyanov",
                    Email = "Mephisto_1999@mail.bg",
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                var result = (await sut.GetAllValidUsersAsync()).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Email, result[i].Email);
                }
            }
        }
    }
}
